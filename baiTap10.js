var array = [];

handleResult = () => {
  var numEl = document.querySelector("#num").value * 1;
  array.push(numEl);
  document.querySelector("#result").innerHTML = `<h1>Mảng số: ${array}</h1>`;
};

handleCompare = () => {
  for (var n = 0, p = 0, i = 0; i < array.length; i++)
    array[i] > 0 ? p++ : array[i] < 0 && n++;
  document.querySelector("#result2").innerHTML =
    p > n
      ? "Số dương > Số âm"
      : n > p
      ? "Số âm > Số dương"
      : "Số âm = Số dương";
};
