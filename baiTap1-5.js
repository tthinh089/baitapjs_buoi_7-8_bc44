var array = [];
handleResult = () => {
  var numEl = document.querySelector("#num").value * 1;
  array.push(numEl);

  var sum = 0;
  var count = 0;
  var posMin = array[0];
  var min = array[0];
  var evenNum = -1;

  for (i = 0; i < array.length; i++) {
    var currentNum = array[i];
    if (currentNum > 0) {
      sum += currentNum;
      count += 1;
      if (currentNum < posMin) {
        posMin = currentNum;
      }
    }
    if (currentNum < min) {
      min = currentNum;
    }
    if (currentNum % 2 == 0) {
      evenNum = currentNum;
    }
  }
  document.querySelector(
    "#result"
  ).innerHTML = `<h1>Mảng số: ${array} </h1> <h2> - Tổng (các) số dương: ${sum} <br> - Có ${count} số dương trong mảng <br> - Số nhỏ nhất trong mảng: ${min} <br> - Số dương nhỏ nhất trong mảng: ${posMin} <br> - Số chẵn cuối cùng trong mảng: ${evenNum}</h2>`;
};
